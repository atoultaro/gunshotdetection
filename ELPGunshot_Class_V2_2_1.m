
% ELP Gunshot detector Version 2 (2015)

% Part I-1: Re-trainging: feature extraction extension
% Use the result of previous running to add training datasets

% Author: Yu Shiu
% Date: 2015/06/08

% Sources of new training datasets
% Random onsets for negative
% FP & TP from the sel tables
% TN from comparison between the original and new sel tables


% How to know the performance is better, if we have no truth data?
clear;

SelTableFolder = 'N:\users\yu_shiu_ys587\__ELP\__ELPGunshotTable\__20150605';
SelTableList = dir(fullfile(SelTableFolder,'kp*.txt'));

[B,A] = butter(10,[.05, .5]);

% Parameters
MelfccParam.WinTime = 0.05;
MelfccParam.HopTime = 0.025;
MelfccParam.MaxFreq = 1000;

warning('off');

%FeaMat1 = [];
FeaMatTot1 = [];
LabelTot1 = [];
for ss = 1:size(SelTableList)
    fprintf('Sound: %s\n', SelTableList(ss,1).name);
    
    % Read the selection table
    EventTable = readtable(fullfile(SelTableFolder, SelTableList(ss,1).name), 'Delimiter','\t');
    % Create truth labels
    Label = strcmp(EventTable{:,{'truepos'}},'TP')-1*strcmp(EventTable{:,{'truepos'}},'FP');
    
    SoundTarget = sound_create('File', EventTable{1, {'BeginFile'}} );
    % GunshotFea2: dim 104
    FeaMat = zeros(size(EventTable,1),104);
    for ee = 1:size(EventTable,1)
        % Read the sound
        SamplesTarget = sound_read(SoundTarget, 'time', EventTable{ee, {'BeginTime_s_'}}, 2.0); % sound_read is weird. It can get the samples even when the t1 is way out of the samples.
        SamplesTarget = SamplesTarget - mean(SamplesTarget,1);
        SamplesTarget = filtfilt(B, A, SamplesTarget); % high-pass filter over 200Hz

        % Feature extraction
        MelfccParam.Samplerate = SoundTarget.samplerate;
        FeaMat(ee,:) = GunshotFea2(SamplesTarget, MelfccParam);
        
        disp('');
    end
    FeaMatTot1 = [FeaMatTot1; FeaMat];
    if(size(FeaMatTot1,1)>1633)
        disp('');
    end
    LabelTot1 = [LabelTot1; Label];
end

%save('FeaMat_Fea2_Iter1.mat', 'FeaMatTot1', 'LabelTot1');
