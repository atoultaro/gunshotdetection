# -*- coding: utf-8 -*-
"""
Gunshot detection: Part I - feature extaction

Created on Sat Feb  7 15:09:34 2015

@author: atoultaro

"""

#%%
import numpy as np
import scipy as sp
from scipy.io import wavfile
#from utility import pcm2float
import matplotlib.pyplot as plt
from scikits.audiolab import play
#from scikits.talkbox.features import mfcc
from librosa.feature import mfcc

import os
#import csv
import timeit

import pickle
#import glob

#import wave
#import pyaudio
#import struct

#import scipy.io.wavfile as sciwav
#import scikits.audiolab, pylab, math
#from scipy.io import loadmat
#from sparseFiltering import *

def MyMain(GunClipDir):
    # Debug
    FlagDraw = 0

    # Parameters
    #StepSize = 10
    #WinSize = 256
    #FFTSize = 512
    
    GunClipDir = '/Users/atoultaro/__Gunshot/__Data/__GunSoundDataset2015'
    CsvFile = 'GunInfo2.csv'
    CsvFile2 = os.path.join(GunClipDir, CsvFile)
    GunClipDir = os.path.join(GunClipDir, '__GunSoundDataset20150227')
    #
    GunInfo = np.loadtxt(open(CsvFile2,"rb"),delimiter=",")
    EventNum = GunInfo.shape[0];
    #EventNum = 100;
    #
    #FeaMatAll = np.zeros((22, 78))
    FeaMatAll = np.zeros((GunInfo.shape[0], 78))
    FeaMatAll = np.zeros((EventNum, 78))
    #for ii in range(1, GunInfo.shape[0]+1):
    #
    #for ii in range(1, 1+22):
    for jj in range(EventNum):
    #for jj in range(GunInfo.shape[0]):
        ii = jj + 1
        print 'File: ' +str(ii)+' Score: '+ str(GunInfo[ii-1,2])
        File = GunClipDir+'/'+ str(ii) +'.wav'
        print(File)
        # Read waveform files
        Fs, Sample = wavfile.read(File)

        #
        if (FlagDraw == 1):
            plts1 = plt.subplot(211)
            plts1.clear()
            plts1.plot(Sample)
            plts1.grid(True)
        #
        # Convert 2-byte-interger sampless to double
        ConvRatio = np.power(2, 15)
        Sample2 = Sample.astype(float) / ConvRatio
        Sample2 = Sample2 - np.mean(Sample2)
        
        # alignment based on band-limited onsets
        
        # Short-time Fourier Transform (STFTs)
        """
        Xdb = np.sqrt(np.abs(stft_zeropad(Sample2, Fs, WinSize, FFTSize, StepSize)))
        Xdb = Xdb.T
        
        if(FlagDraw == 1):
            plts2 = plt.subplot(212)
            plts2.clear()
            #plt.imshow(Xdb[0:FFTSize/2+1,:], origin='lower')
            plt.imshow(Xdb[0:129,:], origin='lower', aspect='auto')
        """ 
        # feature extraction using MFCC
        #QuarSec = 0.25*Fs
        #[ceps, mspec] = mfcc(Sample2[0:int(QuarSec),], nwin=WinSize, nfft=FFTSize, fs=Fs)
        #[ceps, mspec] = mfcc(Sample2[0:int(QuarSec),], nwin=WinSize, nfft=FFTSize, fs=Fs)
        #Ceps1 = mfcc(Sample2[0:int(QuarSec),], fs=Fs)
        #mfcc(Sample2[0:int(QuarSec),],sr=Fs, n_mfcc=13, fmax=1200)
        FeaMat=mfcc(Sample2[0:int(0.25*Fs),],sr=Fs, n_mfcc=13, fmax=1500, n_fft=256, hop_length=200)
        FeaMatMean1 = np.mean(FeaMat, axis=1)
        FeaMatStd1 = np.std(FeaMat, axis=1)
        FeaMat = []
        FeaMat=mfcc(Sample2[int(0.25*Fs):int(0.5*Fs),],sr=Fs, n_mfcc=13, fmax=1200, n_fft=256, hop_length=200)
        FeaMatMean2 = np.mean(FeaMat, axis=1)
        FeaMatStd2 = np.std(FeaMat, axis=1)
        FeaMat = []
        FeaMat=mfcc(Sample2[int(0.5*Fs):int(0.75*Fs),],sr=Fs, n_mfcc=13, fmax=1200, n_fft=256, hop_length=200)
        FeaMatMean3 = np.mean(FeaMat, axis=1)
        FeaMatStd3 = np.std(FeaMat, axis=1)
        
        FeaMatAll[ii-1,] = np.hstack((FeaMatMean1, FeaMatStd1, FeaMatMean2, FeaMatStd2, FeaMatMean3, FeaMatStd3))
        
        if(FlagDraw == 1):       
            plt.show()
            plt.pause(1)
            play(Sample2) # play sound in default 44.1kHz sampling rate
        print('')
        
    return FeaMatAll, GunInfo

#%%
def SoundNorm(X):
    X = X - np.mean(X)
    Xext = 5*np.std(X)
    Dx = 2*Xext
    
    if(Dx == 0):
        Y = X
    else:
        Y = (X+Xext)/Dx*2-1

    for t in np.arange(1, len(Y)):
        if(Y[t]>=1):
            Y[t] = 1.0 - 1/pow(2, 15)

    for t in np.arange(1, len(Y)):
        if(Y[t]<-1):
            Y[t] = -1.0 + 1/pow(2, 15)
    return Y

def stft_zeropad(x, fs, WinSize, FFTSize, StepSize):
    ww = sp.hamming(WinSize)
    X = sp.array([sp.fft(np.concatenate([ww*x[i:i+WinSize],np.zeros(FFTSize - WinSize)])) for i in range(0, len(x)-WinSize, StepSize)])
    #ipdb.set_trace()
    return X
 
if(__name__ == '__main__'):
    print 'Start'
    start = timeit.default_timer()
    
    GunClipDir = '/Users/atoultaro/__Gunshot/__Data/__GunSoundDataset2015'    
    FeaMat, GunInfo = MyMain(GunClipDir)
    
    f = open(os.path.join(GunClipDir, 'Gunshot_Fea.pckl'),'w')
    pickle.dump([FeaMat, GunInfo], f)
    f.close()
     
    stop = timeit.default_timer()
    print 'Run time is: %5.3f' % (stop - start)




# Yu's Doghouse
#%% test the package struct.unpack
"""
fs, sig = wavfile.read('data/test_wav_pcm16.wav')
print("sampling rate = {} Hz, length = {} samples, channels = {}".format(fs, *sig.shape))
print(sig)
plot(sig);
   
    # Spectrogram
    CHUNK = 1024
    #for File in glob.glob(GunClipDir+'/*.wav'):
    for ii in range(1, GunInfo.shape[0]+1):
        print 'File: ' +str(ii)+' Score: '+ str(GunInfo[ii-1,2])
        File = GunClipDir+'/'+ str(ii) +'.wav'
        print(File)
        
        wf = wave.open(File, 'rb')
        (nchannels, sampwidth, fs, nframes, comptype, compname) = wf.getparams()
        frames = wf.readframes (nframes * nchannels)
        wf.close()
        #Sam = struct.unpack_from("%dh" % nframes * nchannels, frames)
        Sam = np.array(struct.unpack_from("%dh" % nframes * nchannels, frames))
        Sam = Sam.astype(float)
        print 'Audio clip length: %8.4f' % float(len(Sam)/float(fs))
        print 'Audio clip maximum value: %2.3f' % np.max(np.abs(Sam))
        
        p = pyaudio.PyAudio()
        stream = p.open(format=p.get_format_from_width(wf.getsampwidth()), channels=wf.getnchannels(), rate=wf.getframerate(), output=True)
        stream.start_stream()
        data = wf.readframes(CHUNK)

        while data != '':
            stream.write(data)
            data = wf.readframes(CHUNK)
    
        stream.stop_stream()
        stream.close()
        p.terminate()
        
        
        plt.figure(1)
        #f, axarr = plt.subplot(211)
        plts1 = plt.subplot(211)
        plts1.clear()
        #axarr[0].plot(Sam)
        plt.plot(Sam)
        plt.grid(True)
        #plt.pause(1)        
        
        Sam2 = SoundNorm(Sam)
        print 'Audio clip maximum value: %2.3f' % np.max(np.abs(Sam2))
        
        Xdb = np.sqrt(np.abs(stft_zeropad(Sam2, fs, WinSize, FFTSize, StepSize)))
        Xdb = Xdb.T
        
        #plt.figure(2)
        #plt.imshow(Xdb)
        #axarr[1].imshow(Xdb)
        plts2 = plt.subplot(212)
        plts2.clear()
        #plt.imshow(Xdb, origin='lower')
        plt.imshow(Xdb[0:FFTSize/2+1,:], origin='lower')
        #plt.autoscale(enable=True)        
        
        plt.show()
        plt.pause(1)

        print ''
"""

"""
#%% read truth labels from the file
TruthFile = []
TruthLabel = []
EventCount = 0
with open(CsvFile2, 'rb') as GunCsv:
    GunInfoRow = csv.reader(GunCsv, delimiter='\t')
    for row in GunInfoRow:
        EventCount += 1
        #print ','.join(row)
    print 'Event count: ' + str(EventCount)
    GunInfo = np.zeros(EventCount, 2)
    EventCount2 = 1
    for row in GunInfoRow:
        GunInfo[EventCount2, 1] = str(row[])
        #TruthFile.append(str(row[0]))
        #if float(row[2]) < 0.55: # review/0.5 goes to negative
        #    TruthLabel.append(0.0)
        #else:
        #    TruthLabel.append(1.0)



NumGun = Count1
Count = 0
DimData = 30*30
data = np.zeros((DimData, NumGun))
for File in FileList:
    if(File.endswith('.wav')):
        print '\n'+File
        FileFull = os.path.abspath(GunClipDir+'/'+File)
        print FileFull
              
        x, fs, nbits = scikits.audiolab.wavread(FileFull)
        print 'Audio Length: %8.2f' % (float(len(x))/float(fs))
        # x normalization
        x = SoundNorm(x)
        #Xdb = np.array([[0.0 for i in range(int(math.floor(len(x)/StepSize))-1)] for j in range(WinSize)])
        Xdb = np.sqrt(sp.abs(stft(x, fs, WinSize, StepSize)))
        data[:, Count] = Xdb[10:70:2,18:48].reshape(DimData) # 30x30
        
        img = plt.imshow(Xdb[0:math.floor(WinSize/2),:], origin='lower')
        #img = plt.imshow(Xdb[0:math.floor(WinSize/2),:])
        #img = plt.imshow(data[:,Count].reshape((30,30)), origin = 'lower')
        plt.title('Truth label: '+str(TruthLabel[Count]))
        plt.show()
        
        Count += 1

Count0 = 0    
Count1 = 0 # count for .wav format
Count2 = 0 # count for non-wav formats
FileList = os.listdir(GunClipDir)

# read through wav files and find the number of files, dimensions and etc
for File in FileList:
    #print File
    if(File.endswith('.wav')):
        Count1 = Count1 + 1
        #print "Wav file: " + File
        #print "Count1: %5d" % Count1
    else:
        Count2 = Count2 + 1
        print "Non-Wav file: " + File
        print "Count2: %5d" % Count2
"""

"""
#%% read sound samples from sound files
for File in glob.glob(GunClipDir+'/*.wav'):
    print(File)
    sr, Data = sciwav.read(File)
    wf = wave.open(File, 'rb')
    print str(wf.getparams())
"""



"""
for row in GunReader:
    TruthFile.append(str(row[0]))
    #TruthLabel.append(float(row[2]))
    if float(row[2]) < 0.55: # review/0.5 goes to negative
        TruthLabel.append(0.0)
    else:
        TruthLabel.append(1.0)

wf = wave.open(File, 'rb')
print 'Sample rate: ' + str(wf.getframerate())
print 'Channel number: ' + str(wf.getnchannels())
print 'Sample width (byte): ' + str(wf.getsampwidth())
print 'Num of frames: ' + str(wf.getnframes())
print 'Audio length (sec): '+str(float(wf.getnframes()/wf.getframerate()))
print str(wf.getparams())
data = wf.readframes(chunk)
#raw_input('Please enter to continue...')

#wf.rewind()
#Sam = wf.readframes(6000)

    
    
p = pyaudio.PyAudio()
stream = p.open(format = p.get_format_from_width(wf.getsampwidth()), channels = wf.getnchannels(), rate = wf.getframerate(), output = True)

        #print("sampling rate = {} Hz, length = {} samples".format(Fs, *Sample.shape))
"""