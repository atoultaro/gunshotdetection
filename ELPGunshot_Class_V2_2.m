
% ELP Gunshot detector Version 2 (2015)

% Part I: Feature extraction only

% Author: Yu Shiu
% Date: 2015/02/23

clear;

%% Debugging flag
FlagDraw = 0;

%% Mode
Mode = 1; % read from original seltab where scores are given
%Mode =2; % read from detector output where FP and TP are the output of human experts' opinion

%% Gun data
% GunData201502.mat has: (a) GunInfo, a matrix: t1, t2, score, sound source & duration; (b) GunSound: cell array for the path to the sound
if(Mode == 1)
    GunData = 'C:\ASE_Misc\__Yu_Playground\__ELP\__ELPGunshot2015\GunData20150227.mat';
elseif(Mode ==2)
    GunData = 'C:\ASE_Misc\__Yu_Playground\__ELP\__ELPGunshot2015\GunDataFP_20150626.mat'; % FP
end
load(GunData);

%% Parameters
TimeInsertSec = 1.0;
%[B,A] = butter(20,[.3, .5]); % .3 Nyquist (2k) = 600 Hz; .5 Nyquist = 1,000 Hz 
%[B,A] = butter(20,[.05, .8]); % .1 Nyquist (2k) = 200 Hz; .6 Nyquist = 1,200 Hz 
[B,A] = butter(10,[.05, .5]);
%[B,A] = butter(20,[.1, .5]); 
%[B,A] = butter(20,0.1,'high'); 
%[B,A] = butter(20,0.2,'low');
%[B,A] = butter(20,0.4,'low');
DecimateBy = 50;
%DecimateBy = 20;
%LenRCos = 20;
LenRCos = 10;
WinTemp = hann(2*LenRCos-1);
WinRCos = WinTemp(LenRCos:end,1);

% FFT
Param.StepSize = 80;
Param.FFTSize = 256;
paramFFT = specgram_parameter; %%<<<<<<<<<<<<==== need the function from xbat!
paramFFT.fft = Param.FFTSize; 
paramFFT.hop = Param.StepSize/Param.FFTSize;

% sound output path
%SoundOut = 'C:\ASE_Data\__ELP\__Gunshots2015';
%SoundOut = 'C:\ASE_Data\__ELP\__Gunshots2015\__GunshotSoundClip2015';
%SoundOut2 = 'C:\ASE_Data\__ELP\__Gunshots2015\__GunshotSoundClip2015Aligned';
GunInfo2 = GunInfo;

%% Label
if(Mode == 1)
    Label = GunInfo(:,3);
    Label2 = (Label>=6);
    Label2 = (Label2-.5)*2;
elseif(Mode == 2)
    Label = GunInfo(:,3);
    Label2 = (Label-.5)*2;
else
    fprintf('No appropriate mode found here.\n');
end
%% Feature extraction
%FeaMat = [];
%FeaMat = zeros(size(GunSound,1),78); % Feature 1
FeaMat = zeros(size(GunSound,1),104); % Feature 2
%FeaMat = zeros(size(GunSound,1),208); % Feature 3
%GunSound2 = cell(size(GunSound));
for ii = 1:size(GunSound,1)
%for ii = 1:1000:size(GunSound,1)
%for ii = 2563:2564
    
    %[pathstr,name,ext] = fileparts(filename)
    [~,SoundName,~] = fileparts(GunSound{ii,1});
    %GunSound2{ii,1} = regexprep(GunSound{ii,1}, 'N:\\data\\brp\\', 'N:\\');
    
    %FileInfo = dir(GunSound{ii,1});
    
    if(rem(ii,10)==0)
        fprintf('Event: %d ; Score: %d ; %s\n', ii, Label(ii,1), SoundName);
    end

    SoundTarget = sound_create('File', GunSound{ii,1} );    
    SamplesTarget = sound_read(SoundTarget, 'time', GunInfo(ii,1), 4.0); % sound_read is weird. It can get the samples even when the t1 is way out of the samples.
    SamplesTarget = SamplesTarget - mean(SamplesTarget,1);
    SamplesFilt = filtfilt(B, A, SamplesTarget); % high-pass filter over 200Hz
    
    if(FlagDraw), figure(101); subplot(5,1,1); plot(SamplesTarget); title(sprintf('Original sound samples: NO. %d, Score %d', ii, Label(ii,1))); grid; end
    SamplesRec = abs(SamplesFilt); % full-wave rectification
    SamplesDec = decimate(SamplesRec, DecimateBy);

    if(FlagDraw), figure(101); subplot(5,1,2); plot(SamplesDec); grid; title('Samples Dec'); end;

    ImpulseWin = [WinRCos-1; fliplr(WinRCos)];
    SamplesEnv = zeros(size(SamplesDec));
    Twincos = size(WinRCos,1);
    for jj = Twincos+1:size(SamplesEnv,1)-Twincos
        SamplesEnv(jj,1) = SamplesDec(jj-Twincos:jj+Twincos-1,1)'*ImpulseWin;
    end

    if(FlagDraw), figure(101); subplot(5,1,3); plot(SamplesEnv); grid; title('Samples Env'); end;

    FrontBuffer = round(SoundTarget.samplerate*0.25/DecimateBy)+1;
    [~, Ind] = max(SamplesEnv(FrontBuffer:end,1));
    Ind = Ind + FrontBuffer -1;
    
    %%SamplesAligned = sound_read(SoundTarget, 'time', GunInfo(ii,1)+(Ind-0)*DecimateBy/SoundTarget.samplerate-0.25-0.25, 4.0); % first 0.25 compensates the FrontBuffer of 0.25sec
    %%wavwrite(SamplesAligned, SoundTarget.samplerate, fullfile(SoundOut2, num2str(ii)));
    
    SamplesAligned = sound_read(SoundTarget, 'time', GunInfo(ii,1)+(Ind-0)*DecimateBy/SoundTarget.samplerate-0.25-0.25,2.0); % first 0.25 compensates the FrontBuffer of 0.25sec
    SamplesAligned = SamplesAligned - mean(SamplesAligned,1);
    SamplesAligned = filtfilt(B, A, SamplesAligned);
    
    if(FlagDraw), figure(101); subplot(5,1,4); plot(SamplesAligned); grid; title('Aligned sound samples'); end;

    %% STFT
    if(FlagDraw)
        [SpecMag, ~, ~] = fast_specgram(SamplesAligned, SoundTarget.samplerate, 'norm', paramFFT);
        %if(FlagDraw), figure(105); imagesc(SpecMag(1:Param.FFTSize/2,:).^.25); grid; title('Spectrogram');axis xy; end;
        %if(FlagDraw), figure(101); subplot(5,1,5); imagesc((1:size(SpecMag,2))*Param.StepSize/SoundTarget.samplerate, (1:Param.FFTSize/2)*SoundTarget.samplerate/Param.FFTSize, SpecMag(1:Param.FFTSize/2,:).^.25); grid; title('Spectrogram');axis xy; end;
        figure(101); subplot(5,1,5); imagesc((1:size(SpecMag,2))*Param.StepSize/SoundTarget.samplerate, ((1:Param.FFTSize/4)-1)*SoundTarget.samplerate/Param.FFTSize, SpecMag(1:Param.FFTSize/4,:).^.25); grid; title('Spectrogram');axis xy;
    end

    
    %% Feature 1: ROC AUC is 0.9776
    if 0
    FeaWinStart = 0.15;
    FeaWinSize = 0.25;
    FeaWin = floor([0.15, 0.4, 0.65, 0.90, 1.15]'*SoundTarget.samplerate);
    ww = 1;
    WinTime = 0.05;
    HopTime = 0.025;
    MaxFreq = 1000;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = mean(Cepstra1,2);
    Fea2 = std(Cepstra1,0,2);
    ww = 2;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    ww = 3;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    ww = 4;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    
    Fea1Temp = diff(Fea1, [],2);
    Fea2Temp = diff(Fea2, [],2);
    FeaMat(ii,:) = [Fea1Temp(:);Fea2Temp(:) ];
    end
    
    %% Feature 2
    if 0
    FeaWin = floor([0.15, 0.4, 0.65, 0.90, 1.15]'*SoundTarget.samplerate);
    WinTime = 0.05;
    HopTime = 0.025;
    MaxFreq = 1000;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = mean(Cepstra1,2);
    Fea2 = std(Cepstra1,0,2);
    ww = 2;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    ww = 3;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    ww = 4;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    
    FeaMat(ii,:) = [Fea1(:);Fea2(:) ];
    end
    
    %% Feature 2 function
    %FeaWin = floor([0.15, 0.4, 0.65, 0.90, 1.15]'*SoundTarget.samplerate);
    MelfccParam.Samplerate = SoundTarget.samplerate;
    MelfccParam.WinTime = 0.05;
    MelfccParam.HopTime = 0.025;
    MelfccParam.MaxFreq = 1000;
    FeaMat(ii,:) = GunshotFea2(SamplesAligned, MelfccParam);
    
    %% Feature 3
    if 0
    FeaWinStart = 0.15;
    FeaWinSize = 0.25;
    FeaWin = floor([0.15, 0.4, 0.65, 0.90, 1.15]'*SoundTarget.samplerate);
    ww = 1;
    WinTime = 0.05;
    HopTime = 0.025;
    MaxFreq = 1000;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = mean(Cepstra1,2);
    Fea2 = std(Cepstra1,0,2);
    Cepstra1Diff = diff(Cepstra1,1,2);
    Fea3 = mean(Cepstra1Diff,2);
    Fea4 = std(Cepstra1Diff,0,2);
    
    ww = 2;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    Cepstra1Diff = diff(Cepstra1,1,2);
    Fea3 = [Fea3, mean(Cepstra1Diff,2)];
    Fea4 = [Fea4, std(Cepstra1Diff,0,2)];

    ww = 3;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    Cepstra1Diff = diff(Cepstra1,1,2);
    Fea3 = [Fea3, mean(Cepstra1Diff,2)];
    Fea4 = [Fea4, std(Cepstra1Diff,0,2)];

    ww = 4;
    [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
    Fea1 = [Fea1, mean(Cepstra1,2)];
    Fea2 = [Fea2, std(Cepstra1,0,2)];
    Cepstra1Diff = diff(Cepstra1,1,2);
    Fea3 = [Fea3, mean(Cepstra1Diff,2)];
    Fea4 = [Fea4, std(Cepstra1Diff,0,2)];
    
    FeaMat(ii,:) = [Fea1(:);Fea2(:); Fea3(:); Fea4(:) ];
    end
    
    disp('');

end

if 0
%% in-the-sample
[EstClassTrain, Model] = adaboostScore('train', FeaMat, Label2, 100);
EstScore = adaboostScore('apply', FeaMat, Model);
end

%% Out-of-sample / cross-validation
if(Mode == 1)
    NumFold = 4;
    NumObs = size(FeaMat,1);
    FoldInd = crossvalind('Kfold', NumObs, NumFold);
    ROC_AUC = zeros(NumFold,1);
    PR_AUC = zeros(NumFold,1);

    EstScoreTot = zeros(NumObs,1);
    for pp = 1:NumFold
    %for pp = 3:4    
        [EstClassTrain, Model] = adaboostScore('train', FeaMat(FoldInd~=pp,:), Label2(FoldInd~=pp,:), 100);
        EstScore = adaboostScore('apply', FeaMat(FoldInd==pp,:), Model);
        LabelFold = Label2(FoldInd==pp,1);

        EstScoreTot(FoldInd==pp,1) = EstScore;

        [~,~,~,ROC_AUC(pp,1)] = perfcurve(LabelFold,EstScore,1);
        fprintf('ROC AUC is %.4f\n', ROC_AUC(pp,1));
        [~,~,~,PR_AUC(pp,1)] = perfcurve(LabelFold,EstScore,1, 'xCrit', 'reca', 'yCrit', 'prec');
        fprintf('Prec-Reca AUC is %.4f\n', PR_AUC(pp,1));
    end

    [~,~,~,ROC_AUCTot] = perfcurve(Label2,EstScoreTot,1);
    fprintf('ROC AUC is %.4f\n', ROC_AUCTot);
    [~,~,~,PR_AUCTot] = perfcurve(Label2,EstScoreTot,1, 'xCrit', 'reca', 'yCrit', 'prec');
    fprintf('Prec-Reca AUC is %.4f\n', PR_AUCTot);

    Thre = 0.0;
    TP = sum((EstScoreTot>Thre).*(Label2==1));
    FN = sum((Label2==1)) - TP;
    TN = sum((EstScoreTot<=Thre).*(Label2==-1));
    FP = sum((Label2==-1)) - TN;
    fprintf('\t\tTP\t\tFN\t\tTN\t\tFP\n');
    fprintf('\t\t%d\t%d\t%d\t%d\n', TP, FN, TN, FP);
    fprintf('\t\t%.2f%%\t%.2f%%\t%.2f%%\t%.2f%%\n', TP/(TP+FN)*100.0, FN/(TP+FN)*100.0, TN/(TN+FP)*100.0, FP/(TN+FP)*100.0);
    
    % F1
    Prec = TP/(TP+FP);
    Recall = TP/(TP+FN);
    fprintf('F1 score: %.4f\n', sqrt(Prec*Recall));
end

%% In-the-samples
% ROC AUC is 0.9841
% Prec-Reca AUC is 0.9352
%		TP		FN		TN		FP
%		3719	516	15056	390
%		87.82%	12.18%	97.48%	2.52%

% Gunshot: 4235; Non-gun: 15446

%% Out-of-samples
% Run 1
% ROC AUC is 0.9776
% Prec-Reca AUC is 0.9077
%
%		TP		FN		TN		FP
%		3636	599	15015	431
%		85.86%	14.14%	97.21%	2.79%

% Run 2
% ROC AUC is 0.9775
% Prec-Reca AUC is 0.9073
%		TP		FN		TN		FP
%		3675	560	15003	443
%		86.78%	13.22%	97.13%	2.87%

% Feature 2
% ROC AUC is 0.9819
% Prec-Reca AUC is 0.9327
% ROC AUC is 0.9816
% Prec-Reca AUC is 0.9223
%		TP		FN		TN		FP
%		3654	581	15124	322
%		86.28%	13.72%	97.92%	2.08%
%F1 score: 0.8905

% Feature 3
% ROC AUC is 0.9787
% Prec-Reca AUC is 0.9052
% ROC AUC is 0.9821
% Prec-Reca AUC is 0.9208
%		TP		FN		TN		FP
%		3707	528	15095	351
%		87.53%	12.47%	97.73%	2.27%
% F1 score: 0.8942




% 5 variables
% RealGunshotSoundSelectionTable 1930x24
% NotGunshotSoundSelectionTable 3347x24

% GunshotSoundSelectionTable_KP07 6739x11
% GunshotSoundSelectionTable_KP05 5683x13

% KorupGunshotSoundSelectionTable 1021x14


%% find the sounds: divide & conquer
% 








% Use data importer


%[SelTab2.YesNum, SelTab2.YesStr] = SelTabExtractELP2(GunYes2);
%[SelTab2.NoNum, SelTab2.NoStr] = SelTabExtractELP2(GunNo2);


%% Classiifcation: feature extraction


%% Classification: cross-validation


%% Classification: model training for deployment use


%% Deployment: onset detection


%% Deployment: feature extraction


%% Deployment: classification


%% Deployment: write out the results

disp('');


if 0
    if 0
%% Classiifcation: Gather gunshot examples: two sets: original and 1st-pass result
%% Collect the gunshot/non-gunshot samples from selection tables
%FolderSel = 'C:\ASE_Data\__ELP\__Gunshots\__Korup';
FolderSel = 'C:\Users\ys587\Box Sync\__ELP\__Gunshot';
GunYes = fullfile(FolderSel, 'RealGunshot_SoundSelectionTable.csv');
GunNo = fullfile(FolderSel, 'NotGunshot_SoundSelectionTable.csv');

[SelTab1.YesNum, SelTab1.YesStr] = SelTabExtractELP1(GunYes);
[SelTab1.NoNum, SelTab1.NoStr] = SelTabExtractELP1(GunNo);



%FolderSel2 = 'N:\mv\Work\YuShiu\__ELPGunshotTable\__201412';
GunYes2 = fullfile(FolderSel, 'KP05_sept2013_concat_1d0_thresh-1p5_SST_tagged.txt');
GunNo2 = fullfile(FolderSel, 'KP07_sep2013_concat_1d0_thresh-1p5_SST_tagged.txt');
    end

%function [SelTabNum, SelTabStr] = SelTabExtractELP1(SelTabFilename)
[fid, ~] = fopen(SelTabFilename,'r');
%Header = textscan(fid,'%s %s %s %s %s %s %s %*[^\n]',1, 'delimiter','\t');
%Values = textscan(fid,'%*d %*s %*f %f %f %f %f %*[^\n]','delimiter','\t');
%Header = textscan(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',1, 'delimiter','\t');
Header = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s',1, 'delimiter','\t');
%Values = textscan(fid,'%d %s %d %f %f %f %f %s %s %f %s %s %f %s %s %d %f %f %s %s %f %s %d %s','delimiter','\t');
Values = textscan(fid,'%f %s %f %f %f %f %f %s %s %f %s %s %f %s %s %f %f %f %s %s %f %s %f %s','delimiter','\t');
fclose(fid);

disp('');

IndNum = [1,3,4,5,6,7,10,13,16,17,18,21,23];
SelTabNum = []; % matrix
for kk = 1:size(IndNum,2)
    SelTabNum = [SelTabNum, Values{1,IndNum(1,kk)}];
end

IndStr = [2,8,9,11,12,14,15,19,20,22,24];
SelTabStr = []; % cell
for kk = 1:size(IndStr,2)
    SelTabStr = [SelTabStr, Values{1,IndStr(1,kk)}];
end


%function [SelTabNum, SelTabStr] = SelTabExtractELP2(SelTabFilename)
[fid, ~] = fopen(SelTabFilename,'r');
%Header = textscan(fid,'%s %s %s %s %s %s %s %*[^\n]',1, 'delimiter','\t');
%Values = textscan(fid,'%*d %*s %*f %f %f %f %f %*[^\n]','delimiter','\t');
%Header = textscan(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',1, 'delimiter','\t');
Header = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s',1, 'delimiter','\t');
%Values = textscan(fid,'%d %s %d %f %f %f %f %s %s %f %s %s %f %s %s %d %f %f %s %s %f %s %d %s','delimiter','\t');
%Values = textscan(fid,'%f %s %f %f %f %f %f %s %f %s %f %f %s','delimiter','\t');
Values = textscan(fid,'%f %s %f %f %f %f %f %s %f %s %f %f %s', 'delimiter','\t');
fclose(fid);

disp('');

IndNum = [1,3,4,5,6,7,10,13,16,17,18,21,23];
SelTabNum = []; % matrix
for kk = 1:size(IndNum,2)
    SelTabNum = [SelTabNum, Values{1,IndNum(1,kk)}];
end

IndStr = [2,8,9,11,12,14,15,19,20,22,24];
SelTabStr = []; % cell
for kk = 1:size(IndStr,2)
    SelTabStr = [SelTabStr, Values{1,IndStr(1,kk)}];
end

end

