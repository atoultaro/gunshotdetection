function FeaVec = GunshotFea2(Samples, MelfccParam)

FeaWin = floor([0.15, 0.4, 0.65, 0.90, 1.15]'*MelfccParam.Samplerate);

ww=1;
[Cepstra1,~,~] = melfcc(Samples(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), MelfccParam.Samplerate, 'wintime',MelfccParam.WinTime,'hoptime',MelfccParam.HopTime, 'maxfreq', MelfccParam.MaxFreq);
Fea1 = mean(Cepstra1,2);
Fea2 = std(Cepstra1,0,2);
ww = 2;
[Cepstra1,~,~] = melfcc(Samples(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), MelfccParam.Samplerate, 'wintime',MelfccParam.WinTime,'hoptime',MelfccParam.HopTime, 'maxfreq', MelfccParam.MaxFreq);
Fea1 = [Fea1, mean(Cepstra1,2)];
Fea2 = [Fea2, std(Cepstra1,0,2)];
ww = 3;
[Cepstra1,~,~] = melfcc(Samples(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), MelfccParam.Samplerate, 'wintime',MelfccParam.WinTime,'hoptime',MelfccParam.HopTime, 'maxfreq', MelfccParam.MaxFreq);
Fea1 = [Fea1, mean(Cepstra1,2)];
Fea2 = [Fea2, std(Cepstra1,0,2)];
ww = 4;
[Cepstra1,~,~] = melfcc(Samples(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), MelfccParam.Samplerate, 'wintime',MelfccParam.WinTime,'hoptime',MelfccParam.HopTime, 'maxfreq', MelfccParam.MaxFreq);
Fea1 = [Fea1, mean(Cepstra1,2)];
Fea2 = [Fea2, std(Cepstra1,0,2)];

FeaVec = [Fea1(:);Fea2(:) ];

return; 