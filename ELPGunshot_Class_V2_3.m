
% ELP Gunshot detector Version 2 (2015)

% Part II: Cross-validation

% Author: Yu Shiu
% Date: 2015/03/03

clear;

%GunFolder = 'C:\ASE_Data\__ELP\__Gunshots2015'; % Change this to your working folder and put the needed file here
GunFolder = 'C:\ASE_Misc\__Yu_Playground\__ELP\__ELPGunshot2015';

if 0
% Truth Label
GunInfoData = fullfile(GunFolder,'GunData20150227.mat');
load(GunInfoData); % variable GunInfo (start time, end time, score, event ID, dataset source)
Score = GunInfo(:,3);
% Expt 1: use all clips in cross-validation, including score 6
Label = Score>=6;
Label = (Label-.5)*2; % adaboost classifier needs the binary labels (-1 vs +1)
end

% Feature vector / matrix
FeaMatData = fullfile(GunFolder,'FeaMat_Fea2FP.mat');
load(FeaMatData); % variable FeaMat
FeaMatFP = FeaMat;
LabelFP = Label;

FeaMatData = fullfile(GunFolder,'FeaMat_Fea2.mat');
load(FeaMatData); % variable FeaMat

FeaMat = [FeaMat;FeaMatFP];
Label = [Label; LabelFP];



[NumObs, NumFea] = size(FeaMat); % number of observation & dimension of feature

% Gunsound
%SoundOut2 = fullfile(GunFolder,'__GunshotSoundClip2015Aligned'); % Original sounds are not included







% Random samples for training/testing set
CVFold = 4;

SeedNum = 10; % The seed controling the randomness; using the same seed guarantees the same results
rng(SeedNum);

RandSeqAll = randperm(NumObs)';
FoldSize = floor(NumObs/CVFold);
CVDataInd = zeros(FoldSize, CVFold);
for jj = 1:CVFold
    CVDataInd(:,jj) = RandSeqAll(jj:CVFold:(FoldSize-1)*CVFold+jj,1);
end

%% Cross-validation of the classifier
AUC = zeros(CVFold,1);
figure(51);
for jj = 1:CVFold
    
    TestInd = CVDataInd(:,jj);
    TrainInd = [];
    for kk = 1:CVFold
        if(kk ~= jj)
            TrainInd = [TrainInd; CVDataInd(:,kk)];
        end
    end

    %% Train
    % train by adaboost
    [EstClass, TrainedModel] = adaboostScore('train', FeaMat(TrainInd,:), Label(TrainInd,1), 100);

    %% Test
    EstScore = adaboostScore('apply', FeaMat(TestInd,:), TrainedModel); % <<<<<<<<<<========= need the function!
    
    [X,Y,~,AUC(jj,1)] = perfcurve(Label(TestInd,1),EstScore,1); % needs stat toolbox
    fprintf('AUC: %.4f\n', AUC(jj,1));
    subplot(2,2,jj); plot(X,Y); grid; title(sprintf('Fold=%d, AUC=%.4f', jj, AUC(jj,1)));
    
    disp('')
end

if 0 % Use all data to train a classifier
%% Training for a model using all data
[EstClass, TrainedModel] = adaboostScore('train', FeaMat, Label, 100);
EstScore = adaboostScore('apply', FeaMat, TrainedModel);
[X,Y,~,AUC_Now] = perfcurve(Label,EstScore,1);

FeaMat = [FeaMat; FeaMatTot1];
Label = [Label; LabelTot1];

[X,Y,~,AUC_Now] = perfcurve(Label(19682:end,1),EstScore(19682:end,1),1);

save('GunshotModel20141114_Onset90.mat','TrainedModel');
end
