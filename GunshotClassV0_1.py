# -*- coding: utf-8 -*-
"""
Gunshot detection: Part II - cross validation

Created on Fri Mar  6 15:48:18 2015

@author: atoultaro
"""

import timeit
import pickle
import os
import numpy as np

from sklearn import cross_validation as cv
#from sklearn import svm
from sklearn.ensemble import RandomForestClassifier

def MyMain():
    print 'Start'
    start = timeit.default_timer()
    
    
    GunClipDir = '/Users/atoultaro/__Gunshot/__Data/__GunSoundDataset2015'
    f = open(os.path.join(GunClipDir, 'Gunshot_Fea.pckl'))
    #pickle.dump([FeaMat, GunInfo], f)
    FeaMat, GunInfo = pickle.load(f)
    f.close()
    
    #Cross-validation
    TruthLabel = (GunInfo[:,2]>=6).astype(float)
    #TruthLabel = 2.0*((GunInfo[:,2]>=6).astype(float)-0.5)
    #
    #clf = svm.SVC(kernel='rbf', C=1)
    #clf = svm.SVC(kernel='linear', C=1, class_weight={1:2.0, 0:1.0}, probability=True)
    clf = RandomForestClassifier(n_estimators=100)
    
    # np.where(np.diff(GunInfo[:,4])==1)[0]
    # sum((GunInfo[6738:12421,2]==0))  ==>> 5542
    # sum((GunInfo[6738:12421,2]>0))  ==>> 141
    # [ 6738, 12421, 15768, 17698] 19681
    # Date 1: -:6559; +:179
    # Date 2: -:5541; +:141
    # Date 3: -:3346; +: 0
    # Date 4: -: 0; +: 1929    
    # Date 5 -: 0; +: 1982  
    #TruthLabel2 = TruthLabel[0:10000,]
    TruthLabel2 = TruthLabel[0:12422,] # Data 1 & 2
    #TruthLabel2 = TruthLabel
    #FeaMat2 = FeaMat[0:10000,:]
    FeaMat2 = FeaMat[0:12422,:]
    #FeaMat2 = FeaMat
    
    #cv = cross_validation.ShuffleSplit(n_samples, n_iter=3, test_size=0.3, random_state=0)    
    cv1 = cv.ShuffleSplit(TruthLabel2.shape[0], n_iter=5, test_size=0.3, random_state=0)    
    
    #scores = cv.cross_val_score(clf, FeaMat2, TruthLabel2, scoring='roc_auc', cv=100, verbose=100)
    #scores = cv.cross_val_score(clf, FeaMat2, TruthLabel2, scoring='roc_auc', cv=5, verbose=100, n_jobs=-1)
    ##scores = cv.cross_val_score(clf, FeaMat2, TruthLabel2, scoring='roc_auc', cv=cv1, verbose=100, n_jobs=-1)
    scores = cv.cross_val_score(clf, FeaMat2, TruthLabel2, scoring='average_precision', cv=cv1, verbose=100, n_jobs=-1)
    #scores = cv.cross_val_score(clf, FeaMat, TruthLabel, scoring='roc_auc', cv=4)
    
    stop = timeit.default_timer()
    print 'Run time is: %5.3f' % (stop - start)
    
    return scores


if (__name__ == '__main__'):
    Scores = MyMain()
    