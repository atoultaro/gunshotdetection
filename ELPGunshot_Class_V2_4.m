
% ELP Gunshot detector Version 2 (2015)

% Part III: Production run/testing the detection / classifier using real sound stream data

% Author: Yu Shiu
% Date: 2015/06/05

clear;

if 0
ii = 1; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp01_sep2014\kp01_20141221_000000.wav';
ii = 2; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp03_mar2014\kp03_20140325_000000.wav';
ii = 3; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp03_mar2014\kp03_20140327_000000.wav';
ii = 4; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp05_mar2014\kp05_20140408_000000.wav';
ii = 5; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp05_mar2014\kp05_20140415_000000.wav';
ii = 6; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp07_mar2014\kp07_20140410_000000.wav';
ii = 7; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp07_mar2014\kp07_20140428_000000.wav';
ii = 8; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp09_mar2014\kp09_20140409_000000.wav';
ii = 9; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp09_sep2014\kp09_20141126_000000.wav';
ii = 10; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp10_mar2014\kp10_20140402_000000.wav';
end

if 0
load('C:\ASE_Misc\__Yu_Playground\__ELP\__ELPGunshot2015\NewSoundName.mat'); % NewSoundName
GunSound = NewSoundName;
end

ii = 1; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp01_mar2014\kp01_20140323_000000.wav';
ii = 2; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp01_mar2014\kp01_20140326_000000.wav';
ii = 3; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_jun2014\kp01_jun2014\kp01_20140703_000000.wav';
ii = 4; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp01_sep2014\kp01_20141217_000000.wav';
ii = 5; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp01_sep2014\kp01_20141221_000000.wav';
ii = 6; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp01_sep2014\kp01_20141223_000000.wav';
ii = 7; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp03_mar2014\kp03_20140326_000000.wav';
ii = 8; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_jun2014\kp03_jun2014\kp03_20140813_000000.wav';
ii = 9; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp03_sep2014\kp03_20141209_000000.wav';
ii = 10; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp05_mar2014\kp05_20140421_000000.wav';
ii = 11; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp05_sep2014\kp05_20141121_000000.wav';
ii = 12; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp07_mar2014\kp07_20140415_000000.wav';
ii = 13; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp07_sep2014\kp07_20141008_000000.wav';
ii = 14; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp07_sep2014\kp07_20141128_000000.wav';
ii = 15; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp09_mar2014\kp09_20140422_000000.wav';
ii = 16; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_sep2014\kp09_sep2014\kp09_20141113_000000.wav';
ii = 17; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_mar2014\kp10_mar2014\kp10_20140424_000000.wav';
ii = 18; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_jun2014\kp10_jun2014\kp10_20140728_000000.wav';
ii = 19; GunSound{ii,1} = 'N:\ELP\ELP2\SOUND FILES\KorupSounds_2014\korup_jun2014\kp10_jun2014\kp10_20140827_000000.wav';
ii = 20; GunSound{ii,1} = 'N:\elp\elp2\sound files\korupsounds_2014\korup_sep2014\kp10_sep2014\kp10_20141022_000000.wav';
ii = 21; GunSound{ii,1} = 'N:\elp\elp2\sound files\korupsounds_2014\korup_sep2014\kp10_sep2014\kp10_20141024_000000.wav';

ModelFile = 'C:\ASE_Misc\__Yu_Playground\__ELP\__ELPGunshot2015\TrainedModel_Fea2.mat'; % trained classifier TrainedModel
%LogDir = 'C:\ASE_Misc\__Yu_Playground\__ELP\__ELPGunshot2015';
LogDir = 'N:\users\yu_shiu_ys587\__ELP\__ELPGunshotTable\__20150626';
%[EstClass, TrainedModel] = adaboostScore('train', FeaMat(TrainInd,:), Label(TrainInd,1), 100);
load(ModelFile); % TrainedModel

%% Parameters
PageSize = 10*60; % sec

[B,A] = butter(10,[.05, .5]); % 100 Hz - 1,000 Hz as SR = 4kHz
DecimateBy = 50; % convert 4,000 to 100 samples
LenRCos = 10; % 0.1 sec
WinTemp = hann(2*LenRCos-1);
WinRCos = WinTemp(LenRCos:end,1);

% FFT
Param.StepSize = 80;
Param.FFTSize = 256;
paramFFT = specgram_parameter; %%<<<<<<<<<<<<==== need the function from xbat!
paramFFT.fft = Param.FFTSize; 
paramFFT.hop = Param.StepSize/Param.FFTSize;

if 0
GunFolder = 'C:\ASE_Data\__ELP\__Gunshots2015';
FeaMatData = fullfile(GunFolder,'FeaMat.mat');
load(FeaMatData); % variable FeaMat
EstScore = adaboostScore('apply', FeaMat, TrainedModel);
end


for ii = 1:size(GunSound,1)
%for ii = 1:5:size(GunSound,1)
    SoundTarget = sound_create('File', GunSound{ii,1} );

    Thre0 = [];
    %ThreCom = 0;
    [~, FileName, ~] = fileparts(GunSound{ii,1});
    fprintf('Processing %s...\n', FileName);
    FileID = fopen(fullfile(LogDir,[FileName,'.txt']),'w');
    fprintf(FileID,'Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\n');
    EventCount = 0;
    for pp = 1:floor(SoundTarget.duration/PageSize)+1
    %for pp = 1:100:floor(SoundTarget.duration/PageSize)+1
    %for pp = 1:2
        %fprintf('Page: %d\n', pp);
        fprintf('#');
        SamplesTarget = sound_read(SoundTarget, 'time', (pp-1)*PageSize, PageSize,1); % sound_read is weird. It can get the samples even when the t1 is way out of the samples.
        SamplesTarget = SamplesTarget - mean(SamplesTarget,1);

        SamplesFilt = filtfilt(B, A, SamplesTarget); % high-pass filter over 200Hz
        SamplesRec = abs(SamplesFilt); % full-wave rectification
        SamplesDec = decimate(SamplesRec, DecimateBy);

        ImpulseWin = [WinRCos-1; fliplr(WinRCos)];
        SamplesEnv = zeros(size(SamplesDec));
        Twincos = size(WinRCos,1);
        %for jj = Twincos+1:size(SamplesEnv,1)-Twincos
        %    SamplesEnv(jj,1) = SamplesDec(jj-Twincos:jj+Twincos-1,1)'*ImpulseWin;
        %end
        SamplesPulse = conv(SamplesDec, ImpulseWin,'same');

        Thre0 = [Thre0; std(SamplesPulse)*3]; 
        if(size(Thre0,1)>=3)
            Thre = mean(Thre0(end-2:end,1));
        else
            Thre = mean(Thre0);
        end
        %%disp(Thre);

        PulseInd = find(SamplesPulse > Thre); %%% <<<=== detection points!! 
        PulseInfo = [PulseInd, SamplesPulse(PulseInd,1)];
        % Bounders = find(diff(PulseInd, 2, 1)>0); ==> later!

        % Feature extraction
        FeaMat = zeros(size(PulseInd,1) ,104); % Feature 2
        for tt = 1:size(PulseInd,1) 
        %for tt = 1:100:size(PulseInd,1) 
            SamplesAligned = sound_read(SoundTarget, 'time', (pp-1)*PageSize + (PulseInd(tt,1)-1)*DecimateBy/SoundTarget.samplerate-0.50, 2.0);
            SamplesAligned = SamplesAligned - mean(SamplesAligned,1);
            SamplesAligned = filtfilt(B, A, SamplesAligned);
            [SpecMag, ~, ~] = fast_specgram(SamplesAligned, SoundTarget.samplerate, 'norm', paramFFT);

            %% Feature 2
            FeaWin = floor([0.15, 0.4, 0.65, 0.90, 1.15]'*SoundTarget.samplerate);
            ww = 1;
            WinTime = 0.05;
            HopTime = 0.025;
            MaxFreq = 1000;
            [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
            Fea1 = mean(Cepstra1,2);
            Fea2 = std(Cepstra1,0,2);
            ww = 2;
            [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
            Fea1 = [Fea1, mean(Cepstra1,2)];
            Fea2 = [Fea2, std(Cepstra1,0,2)];
            ww = 3;
            [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
            Fea1 = [Fea1, mean(Cepstra1,2)];
            Fea2 = [Fea2, std(Cepstra1,0,2)];
            ww = 4;
            [Cepstra1,~,~] = melfcc(SamplesAligned(FeaWin(ww,1)+1:FeaWin(ww+1,1),1), SoundTarget.samplerate, 'wintime',WinTime,'hoptime',HopTime, 'maxfreq', MaxFreq);
            Fea1 = [Fea1, mean(Cepstra1,2)];
            Fea2 = [Fea2, std(Cepstra1,0,2)];

            FeaMat(tt,:) = [Fea1(:);Fea2(:) ];

            %figure(275); plot(SamplesAligned); grid;
            disp('');
        end
        EstScore = adaboostScore('apply', FeaMat, TrainedModel);

        % Find those with scores larger than 0.0
        PosInd = find(EstScore > 0.0); % score thre 0.0

        if(~isempty(PosInd))
            %figure(375); plot(EstScore(PosInd,1)); grid

            for ee = 1:size(PosInd, 1)
                EventCount = EventCount + 1;
                StartTime = (pp-1)*PageSize + (PulseInd(ee,1)-1)*DecimateBy/SoundTarget.samplerate-0.50;
                fprintf(FileID,'%d\t%s\t%d\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n', EventCount, 'Spectrogram', 1, StartTime, StartTime+2.0, 50, 100, EstScore(PosInd(ee,1),1));
            end
            disp('');
        end
    end
    fprintf('\n');
    fclose(FileID);
end

%% Onset / pusle / click Detection


%% Classify
% EstScore = adaboostScore('apply', FeaMat(TestInd,:), TrainedModel);