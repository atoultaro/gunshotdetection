
% ELP Gunshot detector Version 2 (2015)

% Author: Yu Shiu
% Date: 2015/02/23

clear;

%% Debugging flag
FlagDraw = 1;

%% Gun data
% GunData201502.mat has: (a) GunInfo, a matrix: t1, t2, score, sound source & duration; (b) GunSound: cell array for the path to the sound
GunData = 'C:\ASE_Data\__ELP\__Gunshots2015\GunData20150227.mat';
load(GunData);

%% Parameters
TimeInsertSec = 1.0;
%[B,A] = butter(20,[.3, .5]); % .3 Nyquist (2k) = 600 Hz; .5 Nyquist = 1,000 Hz 
%[B,A] = butter(20,[.1, .5]); 
[B,A] = butter(20,0.1,'high'); 
%[B,A] = butter(20,0.2,'low');
%[B,A] = butter(20,0.4,'low');
DecimateBy = 50;
%DecimateBy = 20;
%LenRCos = 20;
LenRCos = 10;
WinTemp = hann(2*LenRCos-1);
WinRCos = WinTemp(LenRCos:end,1);

% FFT
Param.StepSize = 80;
Param.FFTSize = 256;
paramFFT = specgram_parameter; %%<<<<<<<<<<<<==== need the function from xbat!
paramFFT.fft = Param.FFTSize; 
paramFFT.hop = Param.StepSize/Param.FFTSize;

% sound output path
%SoundOut = 'C:\ASE_Data\__ELP\__Gunshots2015';
SoundOut = 'C:\ASE_Data\__ELP\__Gunshots2015\__Test';
GunInfo2 = GunInfo;

%% Label
Label = GunInfo(:,3);

%% Feature extraction
FeaMat = [];
GunSound2 = cell(size(GunSound));
SoundCount = 1;
for ii = 1:size(GunSound,1)
    
    
    %[pathstr,name,ext] = fileparts(filename)
    [~,SoundName,~] = fileparts(GunSound{ii,1});
    %GunSound2{ii,1} = regexprep(GunSound{ii,1}, 'N:\\data\\brp\\', 'N:\\');
    
    FileInfo = dir(GunSound{ii,1});
    %if ~exist(GunSound{ii,1},'file')
    if(FileInfo.bytes  < 100e3)
        fprintf('\nEvent %d, %s is extremely small\n',ii, GunSound{ii,1});
        GunInfo2(ii,3) = 0;
        
        fprintf('%s\n', SoundName);
        
        %SoundTarget = sound_create('File', GunSound{ii,1} );        
        %SamplesTarget = sound_read(SoundTarget, 'time', GunInfo(ii,1), 4.0); % sound_read is weird. It can get the samples even when the t1 is way out of the samples.
        %wavwrite(SamplesTarget, SoundTarget.samplerate, fullfile(SoundOut, num2str(ii)));
        
        %disp('');
    else
        
        fprintf('%s\n', SoundName);
        
        SoundTarget = sound_create('File', GunSound{ii,1} );        
        SamplesTarget = sound_read(SoundTarget, 'time', GunInfo(ii,1), 4.0); % sound_read is weird. It can get the samples even when the t1 is way out of the samples.
    
        if(SoundTarget.samplerate == 4000)
            %%wavwrite(SamplesTarget, SoundTarget.samplerate, fullfile(SoundOut, num2str(ii)));
            %wavwrite(SamplesTarget, SoundTarget.samplerate, fullfile(SoundOut, [num2str(ii), '_', SoundName, '_', num2str(SoundCount)]));
        else
            fprintf('Wrong sampling rate: %d for %s', SoundTarget.samplerate, SoundName);
            break;
        end
        
        
        if 1
        SamplesTarget = SamplesTarget - mean(SamplesTarget,1);
        
        if(FlagDraw), figure(101); subplot(5,1,1); plot(SamplesTarget); title(sprintf('Original sound samples: NO %d', ii)); grid; end

        SamplesFilt = filtfilt(B, A, SamplesTarget); % high-pass filter over 200Hz
        SamplesRec = abs(SamplesFilt); % full-wave rectification
        SamplesDec = decimate(SamplesRec, DecimateBy);

        if(FlagDraw), figure(101); subplot(5,1,2); plot(SamplesDec); grid; title('Samples Dec'); end;

        ImpulseWin = [WinRCos-1; fliplr(WinRCos)];
        SamplesEnv = zeros(size(SamplesDec));
        Twincos = size(WinRCos,1);
        for jj = Twincos+1:size(SamplesEnv,1)-Twincos
            SamplesEnv(jj,1) = SamplesDec(jj-Twincos:jj+Twincos-1,1)'*ImpulseWin;
        end

        if(FlagDraw), figure(101); subplot(5,1,3); plot(SamplesEnv); grid; title('Samples Env'); end;

        FrontBuffer = round(SoundTarget.samplerate*0.25/DecimateBy)+1;
        [~, Ind] = max(SamplesEnv(FrontBuffer:end,1));
        Ind = Ind + FrontBuffer -1;

        SamplesAligned = sound_read(SoundTarget, 'time', GunInfo(ii,1)+(Ind-0)*DecimateBy/SoundTarget.samplerate-0.25-0.25,2.0); % first 0.25 compensates the FrontBuffer of 0.25sec
        SamplesAligned = SamplesAligned - mean(SamplesAligned,1);

        if(FlagDraw), figure(101); subplot(5,1,4); plot(SamplesAligned); grid; title('Aligned sound samples'); end;
        
        %% STFT
        [SpecMag, ~, ~] = fast_specgram(SamplesAligned, SoundTarget.samplerate, 'norm', paramFFT);
        %if(FlagDraw), figure(105); imagesc(SpecMag(1:Param.FFTSize/2,:).^.25); grid; title('Spectrogram');axis xy; end;
        if(FlagDraw), figure(101); subplot(5,1,5); imagesc((1:size(SpecMag,2))*Param.StepSize/SoundTarget.samplerate, (1:Param.FFTSize/2)*SoundTarget.samplerate/Param.FFTSize, SpecMag(1:Param.FFTSize/2,:).^.25); grid; title('Spectrogram');axis xy; end;

        SoundCount = SoundCount + 1;
        disp('');
        end
    end

end


% 5 variables
% RealGunshotSoundSelectionTable 1930x24
% NotGunshotSoundSelectionTable 3347x24

% GunshotSoundSelectionTable_KP07 6739x11
% GunshotSoundSelectionTable_KP05 5683x13

% KorupGunshotSoundSelectionTable 1021x14


%% find the sounds: divide & conquer
% 



if 0
if 0


        SamplesTarget = SamplesTarget - mean(SamplesTarget,1);

        fprintf('Score: %d\n', GunInfo(ii,3));

        

        

        disp('');
        end

        if 0


        %% Feature
        % Feature set 3
        QuarSec = 0.25*SoundTarget.samplerate;
        %TenthSec = 0.10*SoundTarget.samplerate;
        [Cepstra1,~,~] = melfcc(SamplesAligned(1:QuarSec,1), SoundTarget.samplerate, 'wintime',0.10,'hoptime',0.05, 'maxfreq', 1200);
        Fea1 = mean(Cepstra1,2);
        Fea2 = std(Cepstra1,0,2);
        [Cepstra2,~,~] = melfcc(SamplesAligned(QuarSec+1:2*QuarSec,1), SoundTarget.samplerate, 'wintime',0.10,'hoptime',0.05, 'maxfreq', 1200);
        Fea3 = mean(Cepstra2,2);
        Fea4 = std(Cepstra2,0,2);
        [Cepstra3,~,~] = melfcc(SamplesAligned(2*QuarSec+1:3*QuarSec,1), SoundTarget.samplerate, 'wintime',0.10,'hoptime',0.05, 'maxfreq', 1200);
        Fea5 = mean(Cepstra3,2);
        Fea6 = std(Cepstra3,0,2);
        if gg==1
            FeaMat(ii,:) = [Fea1',Fea2',Fea3',Fea4', Fea5', Fea6'];
        else
            FeaMat(ii+DataNum(1,1),:) = [Fea1',Fea2',Fea3',Fea4', Fea5', Fea6'];
        end

        end 
end















if 0
%% Classiifcation: Gather gunshot examples: two sets: original and 1st-pass result
%% Collect the gunshot/non-gunshot samples from selection tables
%FolderSel = 'C:\ASE_Data\__ELP\__Gunshots\__Korup';
FolderSel = 'C:\Users\ys587\Box Sync\__ELP\__Gunshot';
GunYes = fullfile(FolderSel, 'RealGunshot_SoundSelectionTable.csv');
GunNo = fullfile(FolderSel, 'NotGunshot_SoundSelectionTable.csv');

[SelTab1.YesNum, SelTab1.YesStr] = SelTabExtractELP1(GunYes);
[SelTab1.NoNum, SelTab1.NoStr] = SelTabExtractELP1(GunNo);



%FolderSel2 = 'N:\mv\Work\YuShiu\__ELPGunshotTable\__201412';
GunYes2 = fullfile(FolderSel, 'KP05_sept2013_concat_1d0_thresh-1p5_SST_tagged.txt');
GunNo2 = fullfile(FolderSel, 'KP07_sep2013_concat_1d0_thresh-1p5_SST_tagged.txt');
end


% Use data importer


%[SelTab2.YesNum, SelTab2.YesStr] = SelTabExtractELP2(GunYes2);
%[SelTab2.NoNum, SelTab2.NoStr] = SelTabExtractELP2(GunNo2);


%% Classiifcation: feature extraction


%% Classification: cross-validation


%% Classification: model training for deployment use


%% Deployment: onset detection


%% Deployment: feature extraction


%% Deployment: classification


%% Deployment: write out the results

disp('');


if 0
%function [SelTabNum, SelTabStr] = SelTabExtractELP1(SelTabFilename)
[fid, ~] = fopen(SelTabFilename,'r');
%Header = textscan(fid,'%s %s %s %s %s %s %s %*[^\n]',1, 'delimiter','\t');
%Values = textscan(fid,'%*d %*s %*f %f %f %f %f %*[^\n]','delimiter','\t');
%Header = textscan(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',1, 'delimiter','\t');
Header = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s',1, 'delimiter','\t');
%Values = textscan(fid,'%d %s %d %f %f %f %f %s %s %f %s %s %f %s %s %d %f %f %s %s %f %s %d %s','delimiter','\t');
Values = textscan(fid,'%f %s %f %f %f %f %f %s %s %f %s %s %f %s %s %f %f %f %s %s %f %s %f %s','delimiter','\t');
fclose(fid);

disp('');

IndNum = [1,3,4,5,6,7,10,13,16,17,18,21,23];
SelTabNum = []; % matrix
for kk = 1:size(IndNum,2)
    SelTabNum = [SelTabNum, Values{1,IndNum(1,kk)}];
end

IndStr = [2,8,9,11,12,14,15,19,20,22,24];
SelTabStr = []; % cell
for kk = 1:size(IndStr,2)
    SelTabStr = [SelTabStr, Values{1,IndStr(1,kk)}];
end


%function [SelTabNum, SelTabStr] = SelTabExtractELP2(SelTabFilename)
[fid, ~] = fopen(SelTabFilename,'r');
%Header = textscan(fid,'%s %s %s %s %s %s %s %*[^\n]',1, 'delimiter','\t');
%Values = textscan(fid,'%*d %*s %*f %f %f %f %f %*[^\n]','delimiter','\t');
%Header = textscan(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',1, 'delimiter','\t');
Header = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s',1, 'delimiter','\t');
%Values = textscan(fid,'%d %s %d %f %f %f %f %s %s %f %s %s %f %s %s %d %f %f %s %s %f %s %d %s','delimiter','\t');
%Values = textscan(fid,'%f %s %f %f %f %f %f %s %f %s %f %f %s','delimiter','\t');
Values = textscan(fid,'%f %s %f %f %f %f %f %s %f %s %f %f %s', 'delimiter','\t');
fclose(fid);

disp('');

IndNum = [1,3,4,5,6,7,10,13,16,17,18,21,23];
SelTabNum = []; % matrix
for kk = 1:size(IndNum,2)
    SelTabNum = [SelTabNum, Values{1,IndNum(1,kk)}];
end

IndStr = [2,8,9,11,12,14,15,19,20,22,24];
SelTabStr = []; % cell
for kk = 1:size(IndStr,2)
    SelTabStr = [SelTabStr, Values{1,IndStr(1,kk)}];
end

end

